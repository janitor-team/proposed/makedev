Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MAKEDEV
Source: ftp.redhat.com (source .rpm from the 5.1 release)
Comment: there are releases 3.x at https://people.redhat.com/nalin/MAKEDEV/

Files: *
Copyright: ?    Nick Holloway <Nick.Holloway@alfie.demon.co.uk>
           1997 Erik Troan <ewt@redhat.com>
           1997 Michael K. Johnson <johnsonm@redhat.com>
License: GPL-2
Comment:
 No explicit copyright is asserted.  Nick Holloway is the earliest author
 recorded in the sourcecode.  I queried him for an explicit statement
 regarding the license status of this work, and this is his reply:
 .
 Date: Tue, 21 Jul 1998 19:57:10 +0100
 From: Nick Holloway <Nick.Holloway@alfie.demon.co.uk>
 Message-Id: <199807211857.TAA19068@alfie.demon.co.uk>
 To: Bdale Garbee <bdale@gag.com>
 Subject: Re: makedev license?
 .
 > I maintain the makedev package for Debian GNU/Linux.  I am about to move
 > from the ill-fated makedev-1.6 to the makedev-2.3.1 derived from your work
 > by the folks at Redhat.  I don't see any evidence of a copyright assertion
 > or explicit license statement in the source.  Your name appears to be the
 > earliest attached to the current sourcecode.  Am I correct in assuming the
 > GPL?  We try to be meticulous about having our base system be compliant
 > with our Debian Free Software Guidelines, so I'd like an explicit statement.
 .
 It was never explicitly released as GPL, as that would have required
 including the file COPYING which would have been much larger than the
 actual MAKEDEV script (I was also too lazy to find out what incantations
 needed to be made).  However, it is intended to be used as anyone sees
 fit, and the statement under "Copying Policy" is "Freely Redistributable"
 (see MAKEDEV.lsm from any of the releases I made).
 .
 The more recent modifications were done by Michael K. Johnson at
 Redhat.  I think the understanding was that he would be taking over the
 maintenance of MAKEDEV (our discussion took place last September).
 .
 The only previous history was (according to an old posting to
 comp.os.linux) that I started with Jim Winstead's script.
 .
 So, as far as I am concerned, it is consistentwith the Debian FSG.
 .
 --
  `O O'  | Home: Nick.Holloway@alfie.demon.co.uk  http://www.alfie.demon.co.uk/
 // ^ \\ | Work: Nick.Holloway@parallax.co.uk

Files: debian/*
Copyright: 1996-1997 Bruce Perens <bruce@pixar.com>
           1997      Andreas Jellinghaus <aj@debian.org>
           1997-2012 Bdale Garbee <bdale@gag.com>
           2004      Marco d'Itri <md@linux.it>
           2012      gregor herrmann <gregoa@debian.org>
           2012      Robert Millan <rmh@debian.org>
           2013      Colin Watson <cjwatson@debian.org>
           2018      Matthias Klumpp <mak@debian.org>
           2020      Holger Levsen <holger@debian.org>
           2022      Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>
           2022      Joao Eriberto Mota Filho <eriberto@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
